<?php

namespace Drupal\Tests\datamaps\Functional;

use Drupal\datamaps\Datamaps;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests 'datamap' element is rendered properly.
 *
 * @group datamaps
 */
class DatamapElementTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datamaps',
    'datamaps_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * Test the returned response contains a map.
   */
  public function testDefault() {
    $this->drupalGet('datamaps-test-default');
    // Check the page loads properly.
    $this->assertSession()->statusCodeEquals(200);
    // Test libraries being attached.
    file_put_contents('/var/www/html/phpunit.dump', $this->getSession()->getPage()->getContent());
    $this->assertSession()->responseContains('d3.min.js');
    $this->assertSession()->responseContains('topojson.min.js');
    $config = $this->config('datamaps.settings');
    $available_maps = $config->get('available_maps');
    $hires = (bool) $config->get('hires');
    $use_cdn = (bool) $config->get('use_cdn');

    foreach ($available_maps as $datamap) {
      // @TODO Should I use a mock here?
      $this
        ->assertSession()
        ->responseContains(Datamaps::getDatamapPath($datamap, $hires, $use_cdn));
    }

    $this->assertSession()->elementExists('#datamaps-test');
  }

}
