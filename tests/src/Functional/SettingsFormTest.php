<?php

namespace Drupal\Tests\datamaps\Functional;

use Drupal\datamaps\Datamaps;
use Drupal\Tests\BrowserTestBase;

/**
 * Test module configuration form.
 *
 * @group datamaps
 * @covers \Drupal\datamaps\Form\SettingsForm
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datamaps',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * User instance.
   *
   * @var \Drupal\user\Entity\User
   */
  private $user;

  /**
   * The form route.
   *
   * @var string
   */
  private $route = 'admin/config/user-interface/datamaps';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'access administration pages',
      'administer datamaps configuration',
    ]);
  }

  /**
   * Test the page exists and can be accessed with proper permissions.
   */
  public function testSettingsPage() {
    // Logged in as authorized should return 200 and
    // the form should be visible.
    $this->drupalLogin($this->user);
    $this->drupalGet($this->route);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();
    // Now as an unauthorized user and check for 403 error code.
    $this->drupalGet($this->route);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test the form is rendered and can be submitted.
   */
  public function testSettingsForm() {
    $this->drupalLogin($this->user);
    $this->drupalGet($this->route);
    $this->assertSession()->statusCodeEquals(200);
    // Test the form elements exist and have defaults.
    $config = $this->container
      ->get('config.factory')
      ->get('datamaps.settings');

    foreach ($config->get('available_maps') as $name) {
      $this->assertSession()->checkboxChecked('available_maps[' . $name . ']');
    }

    $this->assertSession()->checkboxChecked('hires');

    // Test form submission.
    $data = [
      'hires' => FALSE,
    ];
    // Tic checkboxes randomly.
    $values = array_diff(array_keys(Datamaps::getAvailableMaps()), $config->get('available_maps'));
    if (count($values) > 2) {
      $values = array_slice($values, 0, random_int(1, count($values) - 1));
    }

    foreach ($values as $name) {
      $data['available_maps[' . $name . ']'] = $name;
    }

    $this->submitForm($data, t('Save configuration'));
    $this->assertSession()
      ->pageTextContains('The configuration options have been saved.');

    // Re-test the form default values.
    $this->drupalGet($this->route);
    $this->assertSession()->statusCodeEquals(200);
    foreach ($values as $name) {
      $this->assertSession()->checkboxChecked('available_maps[' . $name . ']');
    }
    $this->assertSession()->checkboxNotChecked('hires');
  }

}
