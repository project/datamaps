<?php

namespace Drupal\Tests\datamaps\Kernel;

use Drupal\datamaps\Datamaps;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests dynamic libraries definitions.
 *
 * @group datamaps
 */
class LibrariesRegistryTest extends KernelTestBase {

  /**
   * The libraries discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscovery
   */
  protected $libraryDiscovery;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datamaps',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['datamaps']);
    $this->libraryDiscovery = $this->container->get('library.discovery');
  }

  /**
   * Test libraries being registered.
   *
   * @covers \Drupal\datamaps\Datamaps::getLibrariesDefinitions
   * @covers \Drupal\datamaps\Datamaps::alterLibrariesDefinitions
   */
  public function testLibrariesDefinitions(): void {
    $config = $this->config('datamaps.settings');
    $this->libraryDiscovery->clearCachedDefinitions();
    $libraries = $this->libraryDiscovery->getLibrariesByExtension('datamaps');
    // Check dependencies are properly loaded.
    $this->assertArrayHasKey('d3', $libraries);
    $this->assertEquals($libraries['d3']['version'], Datamaps::D3_VERSION);
    $this->assertArrayHasKey('topojson', $libraries);
    $this->assertEquals($libraries['topojson']['version'], Datamaps::TOPOJSON_VERSION);
    // Check the select libraries are loaded.
    $available_maps = $config->get('available_maps');
    $maps_libraries = array_intersect_key(array_fill_keys($available_maps, TRUE), $libraries);
    $this->assertCount(count($available_maps), $maps_libraries);
    foreach ($available_maps as $name) {
      $this->assertArrayHasKey($name, $libraries);
    }
  }

  /**
   * Test high-resolution variants are loaded if configured.
   *
   * @covers \Drupal\datamaps\Datamaps::hasHires
   * @covers \Drupal\datamaps\Datamaps::getDatamapPath
   */
  public function testHighResolution(): void {
    $config = $this->config('datamaps.settings');
    $config->set('hires', TRUE);
    $this->libraryDiscovery->clearCachedDefinitions();
    $libraries = $this->libraryDiscovery->getLibrariesByExtension('datamaps');
    $available_maps = $config->get('available_maps');
    foreach ($libraries as $name => $definition) {
      if (!in_array($name, $available_maps, TRUE)) {
        continue;
      }

      if (Datamaps::hasHires($name) !== TRUE) {
        continue;
      }

      $data = array_shift($definition['js']);
      $this->assertArrayHasKey('data', $data);
      $this->assertContains('datamaps.' . $name . '.hires', $data['data']);
    }
  }

  /**
   * Test CDN for 3rd-party vendors are loaded.
   *
   * @covers \Drupal\datamaps\Datamaps::getDatamapPath
   */
  public function testCdn(): void {
    $libraries = $this->libraryDiscovery->getLibrariesByExtension('datamaps');
    foreach ($libraries as $definition) {
      $data = array_shift($definition['js']);
      $this->assertArrayHasKey('data', $data);
      $cdn_a = strpos($data['data'], 'cdnjs.cloudflare.com');
      $cdn_b = strpos($data['data'], 'cdn.jsdelivr.net');
      $this->assertTrue($cdn_a !== FALSE || $cdn_b !== FALSE);
    }
  }

  /**
   * Test local copies for 3rd-party vendors are loaded.
   *
   * @covers \Drupal\datamaps\Datamaps::getDatamapPath
   */
  public function testLocalCopies(): void {
    $config = $this->config('datamaps.settings');
    $config->set('use_cdn', FALSE);
    $this->libraryDiscovery->clearCachedDefinitions();
    $libraries = $this->libraryDiscovery->getLibrariesByExtension('datamaps');
    $available_maps = $config->get('available_maps');
    foreach ($libraries as $name => $definition) {
      $data = array_shift($definition['js']);
      $this->assertArrayHasKey('data', $data);
      if (in_array($name, $available_maps, TRUE)) {
        $name = 'datamaps/' . Datamaps::VERSION . '/datamaps.' . $name;
      }

      $this->assertStringStartsWith('modules/contrib/datamaps/assets/3rd-party/' . $name, $data['data']);
    }
  }

}
