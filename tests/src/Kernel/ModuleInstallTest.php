<?php

namespace Drupal\Tests\datamaps\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests module and config installation.
 *
 * @group datamaps
 * @coversNothing
 */
class ModuleInstallTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datamaps',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['datamaps']);
  }

  /**
   * Test module configuration install.
   */
  public function testConfiguration(): void {
    $config = $this->config('datamaps.settings');
    $this->assertContainsOnly('string', $config->get('available_maps'));
    $this->assertCount(1, $config->get('available_maps'));
    $this->assertContains('world', $config->get('available_maps'));
    $this->assertEquals($config->get('hires'), TRUE);
    $this->assertEquals($config->get('use_cdn'), TRUE);
  }

}
