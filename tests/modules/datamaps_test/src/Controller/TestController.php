<?php

namespace Drupal\datamaps_test\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Test controller for rendering datamaps.
 *
 * @codeCoverageIgnore
 */
class TestController extends ControllerBase {

  /**
   * Render a datamap with default options.
   *
   * @return array
   *   Returns a render array.
   */
  public function defaultMap() {
    return [
      '#type' => 'datamap',
      '#attributes' => [
        'id' => 'datamaps-test',
      ],
    ];
  }

}
