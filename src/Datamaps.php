<?php

namespace Drupal\datamaps;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * General purpose class for non-feature specific elements.
 *
 * @todo Add SHASUM for CDN files (should be the same as local).
 */
class Datamaps implements ContainerInjectionInterface {

  public const D3_VERSION = '3.5.3';

  public const TOPOJSON_VERSION = '1.6.9';

  public const VERSION = '0.5.9';

  /**
   * Available high-resolution maps.
   *
   * @var array
   */
  private static $hiresMaps = [
    'all',
    'world',
  ];

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Datamaps constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Returns the libraries definitions.
   *
   * @return array
   *   An array of libraries.
   */
  public function getLibrariesDefinitions(): array {
    $config = $this->configFactory->get('datamaps.settings');
    $libraries = [];

    foreach ($config->get('available_maps') as $name) {
      $libraries[$name] = [
        'remote' => 'https://datamaps.github.io',
        'version' => self::VERSION,
        'license' => [
          'name' => 'MIT',
          'url' => sprintf('https://github.com/markmarkoh/datamaps/blob/v%s/LICENSE', self::VERSION),
          'gpl-compatible' => TRUE,
        ],
        'js' => [],
        'dependencies' => ['datamaps/d3', 'datamaps/topojson'],
      ];

      $hires = (bool) $config->get('hires');
      $use_cdn = (bool) $config->get('use_cdn');
      $datamap_path = self::getDatamapPath($name, $hires, $use_cdn);
      $libraries[$name]['js'] = [
        $datamap_path => [
          'minified' => TRUE,
        ],
      ];
      // Add CDN attributes.
      if ($use_cdn) {
        $libraries[$name]['js'][$datamap_path] += [
          'type' => 'external',
          'attributes' => [
            'crossorigin' => 'anonymous',
          ],
        ];
      }
    }

    return $libraries;
  }

  /**
   * Returns the datamap script.
   *
   * @param string $datamap
   *   The datamap name.
   * @param bool $hires
   *   Use high-resolution variant if available. Defaults to TRUE.
   * @param bool $cdn
   *   Use CDN. Defaults to TRUE.
   *
   * @return string
   *   The datamap script path.
   */
  public static function getDatamapPath(string $datamap, bool $hires = TRUE, bool $cdn = TRUE): string {
    if ($hires && self::hasHires($datamap)) {
      $datamap .= '.hires';
    }

    if ($cdn) {
      return sprintf('https://cdn.jsdelivr.net/gh/markmarkoh/datamaps@v%s/dist/datamaps.%s.min.js', self::VERSION, $datamap);
    }

    return sprintf('/libraries/datamaps/dist/datamaps.%s.min.js', $datamap);
  }

  /**
   * Alter libraries definitions to change CDN with local files.
   *
   * @param array $libraries
   *   An array of module's libraries.
   *
   * @return array
   *   The altered array.
   */
  public function alterLibrariesDefinitions(array $libraries = []): array {
    // Check if we need to use local copy or CDN for vendors.
    $config = $this->configFactory->get('datamaps.settings');
    if (!empty($config->get('use_cdn'))) {
      return $libraries;
    }

    $libraries['d3']['js'] = [
      '/libraries/d3/d3.min.js' => [
        'minified' => TRUE,
      ],
    ];
    $libraries['topojson']['js'] = [
      '/libraries/topojson/build/topojson.min.js' => [
        'minified' => TRUE,
      ],
    ];

    $libraries['script']['dependencies'][] = 'datamaps/datamaps';

    return $libraries;
  }

  /**
   * Get available maps.
   *
   * @return array
   *   An array available maps options.
   */
  public static function getAvailableMaps(): array {
    return [
      'all' => t('All'),
      'world' => t('World'),
      'usa' => t('USA'),
    ];
  }

  /**
   * Check if a given map has high-resolution version.
   *
   * @param string $map
   *   The map key.
   *
   * @return bool
   *   Returns TRUE if the map has high-resolution version.
   */
  public static function hasHires($map): bool {
    return in_array($map, self::$hiresMaps, TRUE);
  }

}
