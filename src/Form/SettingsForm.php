<?php

namespace Drupal\datamaps\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datamaps\Datamaps;

/**
 * Settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'datamaps_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('datamaps.settings');
    $form = parent::buildForm($form, $form_state);
    $form['library_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Library settings'),
      '#description' => $this->t('This settings acts upon how the libraries are registered.'),
      '#tree' => FALSE,
      '#open' => TRUE,
      'available_maps' => [
        '#type' => 'checkboxes',
        '#title' => $this->t('Available maps'),
        '#description' => $this->t('Choose which maps should be available for rendering.'),
        '#options' => Datamaps::getAvailableMaps(),
        '#default_value' => $config->get('available_maps'),
        '#required' => TRUE,
        '#empty_option' => NULL,
      ],
      'hires' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Use high-resolution maps (if available).'),
        '#description' => $this->t("Loads the high-resolution version of a map. Don't forget about the  footprint; e.g. all ~46KB gzip, all high-resolution ~137KB gzip."),
        '#default_value' => $config->get('hires'),
      ],
      'use_cdn' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Use CDN.'),
        '#description' => $this->t('Load libraries using CDN (Content Delivery Network) instead of local files.'),
        '#default_value' => $config->get('use_cdn'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $this->config('datamaps.settings')
      ->set('available_maps', array_keys(array_filter($values['available_maps'])))
      ->set('hires', $values['hires'])
      ->set('use_cdn', $values['use_cdn'])
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'datamaps.settings',
    ];
  }

}
