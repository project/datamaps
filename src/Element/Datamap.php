<?php

namespace Drupal\datamaps\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Renders a Datamap element.
 *
 * Display a datamap using the selected map variant, default is 'world'.
 *
 * For available options check
 * https://github.com/markmarkoh/datamaps/tree/v0.5.9#default-options
 *
 * Usage example:
 * @code
 * $element['datamap'] = [
 *   '#type' => 'datamap',
 *   '#datamaps' => [
 *     'variant' => 'world',
 *     'options' => [...],
 *   ]
 * ];
 * @endcode
 *
 * @todo Add support for custom scope and dataUrl.
 * @todo Add support for variants.
 *
 * @RenderElement("datamap")
 */
class Datamap extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'datamap',
      '#pre_render' => [
        [$class, 'preRender'],
      ],
      '#attributes' => [],
      '#datamaps' => [
        'variant' => 'world',
        'options' => [],
      ],
    ];
  }

  /**
   * Prepares the element for rendering.
   *
   * @param array $element
   *   The element render array.
   *
   * @return array
   *   The element render array.
   */
  public static function preRender(array $element) {
    if (empty($element['#attributes']['id'])) {
      $element['#attributes']['id'] = Html::getUniqueId('datamaps' . time());
    }

    // Define default element options.
    $options = $element['#datamaps']['options'];
    // Define default scope from element default variant.
    $options['scope'] = $element['#datamaps']['variant'];

    // Allow modules change the options for the datamap before passing it
    // to the Javascript layer.
    $selector = $element['#attributes']['id'];
    $hook_arguments = [$selector, $options];
    $hook_datamaps_options = \Drupal::moduleHandler()
      ->invokeAll('datamaps_options', $hook_arguments);

    // Merge element options with altered options from hook_datamaps_options().
    $options = array_merge($options, $hook_datamaps_options);

    // Update element datamaps values after merge with hook_datamaps_options().
    $element['#datamaps']['options'] = $options;
    $element['#datamaps']['variant'] = $options['scope'];
    unset($element['#datamaps']['options']['scope']);

    $element['#attached'] = [
      'drupalSettings' => [
        'datamaps' => [
          [
            'selector' => $selector,
            'options' => $options,
          ],
        ],
      ],
      'library' => [
        'datamaps/' . $options['scope'],
        'datamaps/script',
      ],
    ];

    return $element;
  }

}
