<?php

/**
 * @file
 * Contains datamaps.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\datamaps\Datamaps;

/**
 * Implements hook_help().
 */
function datamaps_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the datamaps module.
    case 'help.page.datamaps':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Integrates Datamaps library into Drupal') . '</p>';
      $output .= '<h4>' . t('3rd-party Javascript') . '</h4>';
      $output .= '<p>' . t('Currently 3rd-party library handling delegated to developers and pipeline tasks since core does not provides an straight forward to do so (check the following <a target="_blank" href=":url">link</a>)', [
        ':url' => 'https://github.com/drupal-composer/drupal-project/issues/278',
      ]);
      $output .= '</p>';
      $output .= '<p>' . t('Therefor libraries are declared using CDN assets with a fallback option to local committed files under <em>./assets/3rd-party/VENDOR/VERSION</em>.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_library_info_build().
 */
function datamaps_library_info_build() {
  /** @var \Drupal\datamaps\Datamaps $obj */
  $obj = Drupal::classResolver(Datamaps::class);
  return $obj->getLibrariesDefinitions();
}

/**
 * Implements hook_library_info_alter().
 */
function datamaps_library_info_alter(&$libraries, $extension) {
  if ($extension === 'datamaps') {
    /** @var \Drupal\datamaps\Datamaps $obj */
    $obj = Drupal::classResolver(Datamaps::class);
    $libraries = $obj->alterLibrariesDefinitions($libraries);
  }
}

/**
 * Implements hook_theme().
 */
function datamaps_theme() {
  return [
    'datamap' => [
      'render element' => 'element',
    ],
  ];
}
