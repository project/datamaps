# DATAMAPS

## INTRODUCTION

This module integrates [Datamaps][1] library into Drupal.

## REQUIREMENTS

None, dependencies are bundled with the module.

## DEPENDENCIES
Required dependencies for the module.

| Library  | Version |
| -------- | ------- |
| D3       | 3.5.3   |
| TopoJSON | 1.6.9   |
| Datamaps | 0.5.9   |

From your terminal run this commands for installation:
`composer require npm-asset/d3:"^3.5"`
`composer require npm-asset/topojson:"^1.6"`
`composer require npm-asset/datamaps:"^0.5"`
`composer require oomphinc/composer-installers-extender`

Link with the steps to follow:
https://www.bounteous.com/insights/2020/04/22/guide-loading-external-javascript-drupal

## INSTALLATION

As any other Drupal (8 or later) module, from your terminal run
`composer require drupal/datamaps:^1.0`

## CONFIGURATION

To access module configuration go to `/admin/config/user-interface/datamaps`
there you can select which datamaps should be available as libraries, whereas
you want to use high-resolution maps (if available) and CDN option for
libraries.

## USAGE

The module provides a render element `datamap` once set will include the proper
libraries and instantiate the map.

```PHP
$element['datamap'] = [
  '#type' => 'datamap',
  '#datamaps' => [
    'variant' => 'usa',
    'options' => [
      'height' => '300px',
      'width' => '300px',
    ],
  ],
];
```

Currently only `world` and `usa` variants are supported. The `world` variant
comes with high-resolution maps.

## LIBRARY SUPPORT

Since [this][2] currently is not possible to declare libraries as dependencies,
thus dependencies are declared using CDN (default) but are included also as
local copies.

| Library  | Version |
| -------- | ------- |
| D3       | 3.5.3   |
| TopoJSON | 1.6.9   |
| Datamaps | 0.5.9   |

## ICON CREDITS

Made by [srip][3] from [www.flaticon.com][4]

[1]: http://datamaps.github.io/
[2]: https://github.com/drupal-composer/drupal-project/issues/278
[3]: https://www.flaticon.com/authors/srip
[4]: www.flaticon.com
