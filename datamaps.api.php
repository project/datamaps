<?php

/**
 * @file
 * Defines hooks API for Datamaps module.
 */

/**
 * Set options for a given datamap selector.
 *
 * @param string $selector
 *   The datamap selector (used to identify multiple datamaps).
 * @param array $options
 *   An array of current options.
 *
 * @return array
 *   The new array of options.
 *
 * @see \Drupal\datamaps\Element\Datamap::preRender
 * @link https://github.com/markmarkoh/datamaps/tree/v0.5.9#default-options
 */
function hook_datamaps_options($selector, array $options): array {
  if ($selector === 'my-datamap') {
    $options['fills'] = [
      'defaultFill' => '#cdd4d4',
    ];
    $options['geographyConfig']['hideHawaiiAndAlaska'] = TRUE;
  }

  return $options;
}
