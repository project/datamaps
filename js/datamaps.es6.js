/**
 * @file
 * Datamaps behaviors.
 */
(({ behaviors }, { datamaps }, o) => {
  behaviors.datamaps = {
    instances: {},
    attach: (context, settings) => {
      datamaps.forEach(({ selector, options }) => {
        o('datamaps', `#${selector}`, context).forEach(function (element) {
          const datamapsOptions = {
            element,
            ...options
          };
          console.log(options);
          behaviors.datamaps.instances[selector] = new Datamap(datamapsOptions);
        });
      });
    }
  };
})(Drupal, drupalSettings, once);
